using System;

class Program
{
   static void Main()
   {
      var person = new Person();

      person.AskInfo();
      Console.WriteLine(person);

      Console.WriteLine();

      person.StoreMoney();
      Console.WriteLine("Dados da conta atualizados:");
      Console.WriteLine(person);

      Console.WriteLine();

      person.TakeMoney();
      Console.WriteLine("Dados da conta atualizados:");
      Console.WriteLine(person);
   }
}

class Person
{
   private int accountNumber;
   private string name;
   private double money;

    public void AskInfo()
    {
      Console.Write("Entre o numero da conta: ");
      accountNumber = int.Parse(Console.ReadLine());

      Console.Write("Entre o titular da conta: ");
      name = Console.ReadLine();

      Console.Write("Havera deposito inicial? (s/n): ");
      if (Console.ReadLine() == "s") {
         Console.Write("Entre o valor do deposito inicial: ");
         money = double.Parse(Console.ReadLine());
      }
    }

    public void StoreMoney()
    {
      double storedMoney;

      Console.Write("Entre um valor para deposito: ");
      storedMoney = double.Parse(Console.ReadLine());
      money += storedMoney;
    }

    public void TakeMoney()
    {
      double takenMoney;

      Console.Write("Entre um valor para saque: ");
      takenMoney = double.Parse(Console.ReadLine());
      money -= takenMoney;
      money -= 5;
    }    
    
    public override string ToString()
    {
       return $"Conta {accountNumber}, Titular: {name}, Saldo: ${money.ToString("F2")}";
    }
}